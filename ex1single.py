#!/usr/bin/python
# ========== Example 1: Single LED ==========

from dotstar import Adafruit_DotStar
import time


#----- Initialize LEDs -----

numpixels = 30 # Number of LEDs in strip
datapin   = 10
clockpin  = 11
leds = Adafruit_DotStar(numpixels, datapin, clockpin)
leds.begin()           # Initialize pins for output
leds.setBrightness(64) # Limit brightness to ~1/4 duty cycle


#----- Turn on first LED -----

leds.setPixelColor(0, leds.Color(64, 00, 00))   # RGB components
leds.show()
