#!/usr/bin/python
# ========== Example 2: Moving Sequence ==========

from dotstar import Adafruit_DotStar
import time


#----- Initialize LEDs -----

numpixels = 30 # Number of LEDs in strip
datapin   = 10
clockpin  = 11
leds = Adafruit_DotStar(numpixels, datapin, clockpin)
leds.begin()           # Initialize pins for output
leds.setBrightness(64) # Limit brightness to ~1/4 duty cycle


#----- Turn on four LEDs in sequence -----

cGreen = leds.Color(00,64,00)
cBlack = leds.Color(00,00,00)

leds.setPixelColor(0, cGreen)
leds.show()
time.sleep(0.5)

leds.setPixelColor(0, cBlack)
leds.setPixelColor(1, cGreen)
leds.show()
time.sleep(0.5)

leds.setPixelColor(1, cBlack)
leds.setPixelColor(2, cGreen)
leds.show()
time.sleep(0.5)

leds.setPixelColor(2, cBlack)
leds.setPixelColor(3, cGreen)
leds.show()
time.sleep(0.5)
