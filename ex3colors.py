#!/usr/bin/python
# ========== Example 3: Colors ==========

from dotstar import Adafruit_DotStar
import time


#----- Initialize LEDs -----

numpixels = 30 # Number of LEDs in strip
datapin   = 10
clockpin  = 11
leds = Adafruit_DotStar(numpixels, datapin, clockpin)
leds.begin()           # Initialize pins for output
leds.setBrightness(64) # Limit brightness to ~1/4 duty cycle


#----- Turn on LEDs with different colors -----

leds.setPixelColor(0, leds.Color(64, 00, 00))   # red
leds.setPixelColor(1, leds.Color(64, 16, 00))   # orange
leds.setPixelColor(2, leds.Color(64, 64, 00))   # yellow
leds.setPixelColor(3, leds.Color(00, 64, 00))   # green
leds.setPixelColor(4, leds.Color(00, 64, 64))   # cyan
leds.setPixelColor(5, leds.Color(00, 00, 64))   # blue
leds.setPixelColor(6, leds.Color(64, 00, 64))   # violet
leds.setPixelColor(7, leds.Color(64, 64, 64))   # white

leds.show()
