#!/usr/bin/python
# ========== Example 4: Brightness ==========

from dotstar import Adafruit_DotStar
import time


#----- Initialize LEDs -----

numpixels = 30 # Number of LEDs in strip
datapin   = 10
clockpin  = 11
leds = Adafruit_DotStar(numpixels, datapin, clockpin)
leds.begin()           # Initialize pins for output
leds.setBrightness(64) # Limit brightness to ~1/4 duty cycle


#----- Slowly increase the white brightness of one LED -----

leds.setPixelColor(0, leds.Color(16, 16, 16))
leds.show()
time.sleep(0.5)

leds.setPixelColor(0, leds.Color(32, 32, 32))
leds.show()
time.sleep(0.5)

leds.setPixelColor(0, leds.Color(64, 64, 64))
leds.show()
time.sleep(0.5)

leds.setPixelColor(0, leds.Color(128, 128, 128))
leds.show()
time.sleep(0.5)
