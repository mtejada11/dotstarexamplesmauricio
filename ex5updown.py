#!/usr/bin/python
# ========== Example 5: Move Up and Down ==========

from dotstar import Adafruit_DotStar
import time


#----- Initialize LEDs -----

numpixels = 30 # Number of LEDs in strip
datapin   = 10
clockpin  = 11
leds = Adafruit_DotStar(numpixels, datapin, clockpin)
leds.begin()           # Initialize pins for output
leds.setBrightness(64) # Limit brightness to ~1/4 duty cycle

cOrange = leds.Color(64,16,00)


#----- Use loop to turn on single LED in sequence in one direction -----

delay = 0.1 #seconds

for i in range(0,8):

        print "position", i

        leds.begin() # reset all 
        leds.setPixelColor(i, cOrange)
        leds.show()
        time.sleep(delay)


#----- Use loop to turn on single LED in sequence in opposite direction -----

for i in range(0,8):

        print "position", 7-i

        leds.begin() # reset all
        leds.setPixelColor(7-i, cOrange)
        leds.show()
        time.sleep(delay)


#----- Reset LED strip to turn all off -----

# begin() command also resets and turns off all LEDs

leds.begin()   
leds.show()
