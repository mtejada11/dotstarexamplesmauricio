#!/usr/bin/python
# ========== Example 6: Blinking ==========

from dotstar import Adafruit_DotStar
import time


#----- Initialize LEDs -----

numpixels = 30 # Number of LEDs in strip
datapin   = 10
clockpin  = 11
leds = Adafruit_DotStar(numpixels, datapin, clockpin)
leds.begin()           # Initialize pins for output
leds.setBrightness(64) # Limit brightness to ~1/4 duty cycle

cOrange = leds.Color(64,16,00)
cBlack  = leds.Color(00,00,00)


#----- Blink first LED four times

for i in range(0,4):

        print "First LED", i

        leds.setPixelColor(0, cOrange)
        leds.show()
        time.sleep(0.2)

        leds.setPixelColor(0, cBlack)
        leds.show()
        time.sleep(0.2)


#----- Blink last LED four times

for i in range(0,4):

        print "Last LED", i

        leds.setPixelColor(7, cOrange)
        leds.show()
        time.sleep(0.2)

        leds.setPixelColor(7, cBlack)
        leds.show()
        time.sleep(0.2)
